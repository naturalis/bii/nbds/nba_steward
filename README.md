# README

The Netherlands Biodiversity Data Services (NBDS) is a set of services centered around one core service, the Naturalis Biodiversity API (NBA). One of the aspects of the NDS is a (semi)automated data flow, from providers of data sets to document store. The Steward application is a set of bash scripts that manages the flow of data between different NBDS components. 

The Steward has four "roles" defined by the location it is deployed:

- Source : here the source files are deliverd by the data providers. When new data has arrived, Steward takes care of creating the job that will finally result in the ingest of new data in the document store.
- ETL : ...
- Transform : ...
- Colander: ...



## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`
