#!/bin/bash

# usage: upload_job.sh job-file.json job-dir-name
#
# E.g.: 

alias aws='docker run --rm -i -v $MINIO_ETL_DIR:/aws \
         --env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID \
         --env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY \
         --env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION \
         --env AWS_BUCKET=$AWS_S3_BUCKET \
         amazon/aws-cli'

shopt -s expand_aliases

log_info() {
  echo "INFO: $1"
}

log_error() {
  let errors="$errors + 1"
  echo -e "ERROR: $1" 1>&2
}

exit_on_error() {
  echo -e "ERROR: $1" 1>&2
  exit 1
}

# Are both job file and source directory available?
if [ ! -s $MINIO_ETL_DIR/tmp/$1 ]; then
  exit_on_error "job file $1 is missing!"
fi

if [ ! -d $MINIO_ETL_DIR/tmp/$2 ]; then
  exit_on_error "directory $2 does not exists!"
fi

# Upload to AWS
error=$( aws s3 sync tmp/$2 $AWS_S3_BUCKET/etl/$2/ )
if [ $? -gt 0 ]; then
  echo "ERROR: upload of $2 to AWS failed: $error"
  exit $?
fi
error=$( aws s3 cp tmp/$1 $AWS_S3_BUCKET/etl/new/$1 )
if [ $? -gt 0 ]; then
  echo "ERROR: upload of $1 to AWS failed: $error"
  exit $?
fi

log_info "job file succesfully uploaded to: $AWS_S3_BUCKET/etl/new/$1"
log_info "source files succesfully uploaded to bucket: $AWS_S3_BUCKET/etl/$2/"