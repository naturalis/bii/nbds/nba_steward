#!/bin/bash
#
# functions.sh : shared functions
#
# Tom Gilissen
# February 2021

function log_debug() {
  if $debug; then
    echo "$(date -Iseconds)|DEBUG|$(basename -- $0): $1"
  fi
}

function log_error() {
  errors=$((errors + 1))
  echo "$(date -Iseconds)|ERROR|$(basename -- $0): $1" 1>&2
}

function log_info() {
  echo "$(date -Iseconds)|INFO |$(basename -- $0): $1"
}

function log_warning() {
  warnings=$((warnings + 1))
  if [ "$warn" ]; then
    echo -e "$(date -Iseconds)|WARN |$(basename -- $0): $1"
  fi
  if [ "$fail_on_warning" ]; then
    exit $warnings
  fi
}

function exit_on_error() {
  log_error "$1"
  exit 1
}
