#!/bin/bash
# 
# run.sh : utility scripts for running the three main tasks
# of this Steward: 
# deploy_dirs.sh   to create or update the directory structure
# prepare_jobs.sh  to check for new data and create jobs if there is
#
# Usage: ./run.sh
# 
# Extra parameter:
# --debug : show debug messages in log
# 
# 
# Tom Gilissen
# Februari 2021

source functions.sh

debug=false

if [ ! -z $1 ] && [ $1 = "--debug" ]; then
  debug=true
fi

log_info "Steward ETL 1/3: deploying etl directories"
deploy_dirs.sh $1

log_info "Steward ETL 2/3: check for medialib update files"
upload_medialib_data.sh $1

log_info "Steward ETL 3/3: check for new transformation jobs"
prepare_jobs.sh $1
