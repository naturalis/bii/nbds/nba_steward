#!/bin/bash
#
# prepare_etl_jobs.sh ...
# 
# Usage: ./prepare_jobs
# 
# Extra parameters:
# --debug : include all possible messages
# --warn  : include warning messages
#
# E.g.: ./prepare_jobs --debug
#
# 
# 
# Tom Gilissen
# Februari 2021

source functions.sh

### Variables and Constants

debug=false
warn=false
errors=0
warnings=0
fail_on_warning=false

declare -a valid_doc_types=(
  multimedia
  specimen
  taxon
  )

# Note: data_suppliers is defined in prepare_jobs.cfg

date_iso_8601=
id=
status=

input_dir=
job_dir=

# default values can be overridden with the config file
# tmp_dir=/data/etl/tmp
# supplier_dir=/data/etl/naturalis
tmp_dir=$MINIO_ETL_DIR/tmp
supplier_dir=$MINIO_ETL_DATA_DIR

CONFIG_FILE=/usr/local/bin/steward/prepare_jobs.cfg

# For testing only:
# source /path/to/.env

### Functions

load_script_props() {
  if [ -e $CONFIG_FILE ]; then
    source $CONFIG_FILE
  fi
}

set_date() {
  date_iso_8601=`date --iso-8601=seconds`
  log_info "job date: $date_iso_8601"
}

set_id() {
  data_supplier=$1
  local timestamp=$(date +%s%3N)
  local miliseconds=${timestamp:(-3)}
  local datetime=$(date +%Y-%m-%d-%H%M%S$miliseconds)
  local hash=$(echo "${data_supplier,,}$timestamp" | sha256sum)
  local hash=${hash:0:8} # use only the first 8 characters
  id=${data_supplier,,}-$datetime-$hash
  log_info "job id: $id"
}

set_doc_type_source() {
  for type in "$@";
  do
    if [ $type == "multimedia" ]; then
      multimedia_source=true
    fi
    if [ $type == "specimen" ]; then
      specimen_source=true
    fi
    if [ $type == "taxon" ]; then
      taxon_source=true
    fi
  done
}

reset_doc_type_source() {
  multimedia_source=false
  specimen_source=false
  taxon_source=false
}

create_new_job_dir() {
  UUID=$( uuidgen )
  job_dir=$tmp_dir/$UUID
  mkdir -p $job_dir
  log_debug "created job dir: $job_dir"
}

# Move all source files to a temporary job folder ($job_dir) out of 
# sight of the user so they cannot be changed any longer
move_source_files() {

  log_debug "input_dir: $input_dir"
  log_debug "job_dir: $job_dir"

  ls $input_dir | grep -v upload_ready | xargs -I {} mv $input_dir/{} $job_dir/
  rm $input_dir/upload_ready

  log_info "source files have been moved to: $job_dir"
}



# Set source data: data dir (source_dir) and data files (source_files)
set_source_data() {
  source_dir=$UUID
  local files=$( ls $job_dir -AH )
  source_files=$( printf "\"%s\"\n" ${files[@]} | jq -s '.' )
}

save_job_file() {
  jq \
      -n --arg id "$id" \
      -n --arg data_supplier "$data_supplier" \
      -n --arg date "$date_iso_8601" \
      -n --arg status "$status" \
      -n --arg tabula_rasa $tabula_rasa \
      -n --arg test_run $test_run \
      -n --arg source_dir "$UUID" \
      -n --argjson source_files "$source_files" \
      $compact \
  '{
    "id": "\($id)",
    "data_supplier": "\($data_supplier)",
    "date": "\($date)",
    "status": "\($status)",
    "tabula_rasa": $tabula_rasa | test("true"),
    "test_run": $test_run | test("true"),
    "source_directory": "\($source_dir)",
    "source_files": $source_files
  }' \
  > $tmp_dir/$1 || exit_on_error "Failed to create job file!"
}


### Main 

load_script_props

## Command Line Parameters

# Extra logging
if [ ! -z $1 ] && [ $1 = "--debug" ]; then
  debug=true
  warn=true
fi
if [ ! -z $1 ] && [ $1 = "--warn" ]; then
  warn=true
fi



if [ ${#data_suppliers} -eq 0 ]; then
  exit_on_error "no data suppliers defined. Check the config file!"
fi


# Loop over supplier folders
for supplier in ${data_suppliers[@]};
do
  log_debug "Data supplier: $supplier"

  input_dir=$supplier_dir/$supplier/new
  # Strip trailing /
  if [ ${input_dir: -1} == "/" ]; then
    input_dir=${input_dir::-1}
  fi
  log_debug "Reading source files from: $input_dir"

  if [ ! -e "$input_dir/upload_ready" ]; then
    log_debug "nothing to do for $supplier"
    continue
  fi

  log_info "creating job for $supplier"

  # Initialize the required job variables
  set_date
  set_id $supplier

  # Move the source files from the input folder to a temporary job folder
  create_new_job_dir
  move_source_files
  set_source_data
  status="IN_PROGRESS"

  # Save the job file
  job_file=$id.json
  save_job_file $job_file

  # Upload job file and source file to AWS S3
  log_debug "running: upload_job.sh $job_file $UUID"
  upload_job.sh $job_file $UUID
  if [ $? -gt 0 ]; then
    log_error "upload of job $id to AWS failed!"
    mv $tmp_dir/$job_file /data/etl/errors/$job_file
    mv $tmp_dir/$UUID /data/etl/errors/.
  else
    log_info "successfully uploaded job $id to AWS"
    mv $tmp_dir/$job_file /data/etl/in_progress/$job_file
    mv $tmp_dir/$UUID /data/etl/naturalis/$supplier/archive/.
  fi

done

log_info "finished with $errors error(s) and $warnings warning(s)"
exit $errors
