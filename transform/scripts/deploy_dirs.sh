#!/bin/bash
#
# deploy_dirs.sh creates the directories necessary for 
# Steward Transformation
#
# Tom Gilissen
# Februari 2021

# shellcheck source=/dev/null
source functions.sh

errors=0

if ! mkdir -p /data/etl/{done,export,incoming,medialib,reports};
then
  log_error "(re)creating ETL directories failed"
fi

if ! mkdir -p /data/etl/incoming/{brahms,col,crs,dcsr,geo,json-imports,medialib,ndff,nsr};
then
  log_error "(re)creating ETL default source-data directories failed"
fi

if ! mkdir -p /data/steward/{done,in_progress,medialib,new,temporary};
then
  log_error "(re)creating Steward Transformation directories failed"
fi

exit $errors
