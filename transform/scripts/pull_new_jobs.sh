#!/bin/bash
# pull_new_jobs.sh
#
# Sync the AWS S3 etl new jobs folder to the local folder:
#
# aws:/etl/new >> /data/steward/incoming
#
# and move the downloaded job file(s) to the in_progress folder.
#
# NOTE: '-v $STEWARD_DATA_DIR:/aws' maps the directory of the host system
# (NOT the steward container!) to the working directory /aws of the aws cli 
# container. However, in order to have access to the data folder inside this 
# steward container, we need to map that host directory to a directory in
# this container as well (we'll use: /data/steward).
#
# Tom Gilissen
# Februari 2021

# shellcheck source=/dev/null
source functions.sh


alias aws='docker run --rm -i -v $STEWARD_DATA_DIR:/aws '\
'--env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID '\
'--env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY '\
'--env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION '\
'--env AWS_BUCKET=$AWS_S3_BUCKET '\
'amazon/aws-cli'

shopt -s expand_aliases

### Constants and variables

# required .env variable:
# STEWARD_DATA_DIR=/data/steward
# $ETL_DATA_DIR=/data/etl


debug=false
warn=false
fail_on_warning=false
errors=0
warnings=0


### Main

# Debug logging
if [ -n "$1" ] && [ "$1" = "--debug" ]; then
  debug=true
fi
if [ -n "$1" ] && [ "$1" = "--warn" ]; then
  warn=true
fi
if [ $debug ]; then
  log_debug "debug: $debug"
  log_debug "warn: $warn"
  log_debug "fail on warning: $fail_on_warning"
fi


# 1. Sync the AWS S3 etl new jobs folder to: /data/steward/new
if ! aws s3 sync "$AWS_S3_BUCKET"/etl/new/ incoming/;
then
  exit_on_error "sync of new jobs folder failed"
fi

# 2. On AWS, move the synched job files to the in_progress folder
# NOTE: the job files on the Steward host remain in the "new" folder!
mapfile -t files < <( ls "$STEWARD_DATA_DIR"/incoming/*.json -atr 2> /dev/null)
if [ ${#files[@]} -gt 0 ]; then
  for file in "${files[@]}"
  do
    file_name=${file#"$STEWARD_DATA_DIR"/incoming/}
    from="$AWS_S3_BUCKET/etl/new/$file_name"
    to="$AWS_S3_BUCKET/etl/in_progress/$file_name"

    mapfile -t result < <(aws s3 ls "$from" 2> /dev/null)
    if [ ${#result[@]} -gt 0 ];
    then
      if ! aws s3 mv "$from" "$to";
      then
        log_error "moving job $file_name in the in_progress folder failed"
      else
        log_debug "moved $file_name to in_progress"
      fi
    fi
  done
fi

log_info "sync of jobs finished with $errors error(s) and $warnings warning(s)"
exit $errors
