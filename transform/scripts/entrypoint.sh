#!/bin/bash
#
# entrypoint.sh
#
# Tom Gilissen
# November 2021


# shellcheck source=/dev/null
source functions.sh

debug=false

log_info "debug status: $debug"

# Verify if the mandatory environment variables have been provided

if [[ -z $AWS_S3_ACCESS_KEY_ID ]]; then
		exit_on_error "Missing env variable: AWS_S3_ACCESS_KEY_ID"
fi
if [[ -z $AWS_S3_SECRET_ACCESS_KEY ]]; then
		exit_on_error "Missing env variable: AWS_S3_SECRET_ACCESS_KEY"
fi
if [[ -z $AWS_HOSTED_ZONE_ID ]]; then
		exit_on_error "Missing env variable: AWS_HOSTED_ZONE_ID"
fi
if [[ -z $AWS_S3_DEFAULT_REGION ]]; then
		exit_on_error "Missing env variable: AWS_S3_DEFAULT_REGION"
fi
if [[ -z $AWS_S3_BUCKET ]]; then
		exit_on_error "Missing env variable: AWS_S3_BUCKET"
fi


# Set the environment variables for all users
{
	echo "AWS_S3_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID"
	echo "AWS_S3_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY"
	echo "AWS_HOSTED_ZONE_ID=$AWS_HOSTED_ZONE_ID"
	echo "AWS_S3_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION"
	echo "AWS_S3_BUCKET=$AWS_S3_BUCKET"
	echo "GITLAB_CONFIG_PRIVATE_KEY_BASE64=$GITLAB_CONFIG_PRIVATE_KEY_BASE64"
	echo "GITLAB_CONFIG_REPO=$GITLAB_CONFIG_REPO"
	echo "ETL_DATA_DIR=/data/etl"
	echo "STEWARD_DATA_DIR=/data/steward"
} >> /etc/environment

# Configure git

# 1. add credentials on build, Certificate environment variable SSH_PRIVATE_KEY is Base64 encoded!
mkdir -p /root/.ssh/
echo "$GITLAB_CONFIG_PRIVATE_KEY_BASE64" | base64 --decode > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa

# 2. make sure your domain is accepted
touch /root/.ssh/known_hosts
ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

# 3. create a dir for logging
mkdir -p /etc/steward/log

# 4. ... and initialise the cron log file
touch /etc/steward/log/steward_transform.log

# Start the run once job
log_info "container has started"

# Setup the cron schedule
crontab /home/ubuntu/crontab
# ... and start cron
cron -f &

# Follow the log file so it shows up in the container log
tail --follow /etc/steward/log/steward_transform.log