#!/bin/bash
# pull_medialib_updates.sh
#
# NOTE: '-v $STEWARD_DATA_DIR:/aws' maps the directory of the host system 
# (NOT the steward container!) to the working directory /aws of the aws cli 
# container. However, in order to have access to the data folder inside this 
# steward container, we need to map that host directory to a directory in
# this container as well (we'll use: /data).
#
# Tom Gilissen
# November 2021


# shellcheck source=/dev/null
source functions.sh


alias aws='docker run --rm -i -v $STEWARD_DATA_DIR:/aws '\
'--env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID '\
'--env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY '\
'--env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION '\
'--env AWS_BUCKET=$AWS_S3_BUCKET '\
'amazon/aws-cli'

shopt -s expand_aliases

# required .env variable:
# STEWARD_DATA_DIR=/data/steward
# $ETL_DATA_DIR=/data/etl

debug=false
warn=false
fail_on_warning=false
errors=0
warnings=0

### Main

# Debug logging
if [ -n "$1" ] && [ "$1" = "--debug" ]; then
  debug=true
fi
if [ -n "$1" ] && [ "$1" = "--warn" ]; then
  warn=true
fi
if [ $debug ]; then
  log_debug "debug: $debug"
  log_debug "warn: $warn"
  log_debug "fail on warning: $fail_on_warning"
fi

# 1. sync the AWS S3 medialib folder to: /data/steward/medialib
if ! aws s3 sync "$AWS_S3_BUCKET/medialib/" "medialib/";
then
  exit_on_error "sync of new medialib folder failed"
fi

# 2. move the most recent update files to the ETL source data folder

# 2a. ids update
mapfile -t files < <( ls "$STEWARD_DATA_DIR"/medialib/*-ids-*.tar.gz -ra 2> /dev/null )
if [ ${#files[@]} -gt 0 ]; then
  # extract the first (most recent) file to the source data directory
  log_info "extracting ids file ${files[0]} to $ETL_DATA_DIR/medialib"
  if ! tar -xzf "${files[0]}" --directory "$ETL_DATA_DIR"/medialib 2> /dev/null;
  then
    exit_on_error "Failed to extract medialib update file: ${files[0]}. No medialib update has been carried out!"
  fi
  # however, if successful, remove all remaining updates
  for file in "${files[@]}"
  do
    # remove both locally as well as from aws
    file_name=${file#"$STEWARD_DATA_DIR/medialib/"}
    log_debug "removing medialib ids update: $file_name"
    if ! aws s3 rm "$AWS_S3_BUCKET/medialib/$file_name"
    then
      log_error "failed to remove file $file_name from aws"
    fi
    if ! rm "$file";
    then
      log_error "failed to remove file $file_name from local file system"
    fi
  done
else
  log_debug "no ids update file: nothing to do"
fi

# 2b. mimetypes update
mapfile -t files < <( ls "$STEWARD_DATA_DIR"/medialib/*-mimetypes-*.tar.gz -ra 2> /dev/null )
if [ ${#files[@]} -gt 0 ]; then

  # extract the first and most recent file to the source data directory
  log_info "extracting mimetypes file ${files[0]} to $ETL_DATA_DIR/medialib"
  if ! tar -xzf "${files[0]}" --directory "$ETL_DATA_DIR"/medialib 2> /dev/null
  then
    exit_on_error "failed to extract medialib update file: ${files[0]}. No medialib update has been carried out"
  fi

  # when successful, all remaining files can be removed
  for file in "${files[@]}"
  do
    # remove both locally as well as from aws
    file_name=${file#"$STEWARD_DATA_DIR/medialib/"}
    log_debug "removing medialib mimetypes update: $file_name"
    if ! aws s3 rm "$AWS_S3_BUCKET/medialib/$file_name";
    then
      log_error "failed to remove file $file_name from aws"
    fi
    if ! rm "$file";
    then
      log_error "failed to remove file $file_name!"
    fi
  done
else
  log_debug "no mimetype update file: nothing to do"
fi

log_info "sync of jobs finished with $errors error(s) and $warnings warning(s)"
exit $errors
