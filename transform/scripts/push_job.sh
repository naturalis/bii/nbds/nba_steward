#!/bin/bash
#
# push_job.sh [jobId]
#
# Tom Gilissen
# Februari 2021


# shellcheck source=/dev/null
source functions.sh


alias aws='docker run --rm -i -v $STEWARD_DATA_DIR:/aws '\
'--env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID '\
'--env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY '\
'--env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION '\
'--env AWS_BUCKET=$AWS_S3_BUCKET '\
'amazon/aws-cli'

shopt -s expand_aliases

### Constants and variables

debug=false
warn=false
fail_on_warning=false
errors=0
warnings=0


### Main

# Debug logging
if [ -n "$1" ] && [ "$1" = "--debug" ]; then
  debug=true
fi
if [ -n "$1" ] && [ "$1" = "--warn" ]; then
  warn=true
fi
if [ $debug ]; then
  log_debug "debug: $debug"
  log_debug "warn: $warn"
  log_debug "fail on warning: $fail_on_warning"
fi

log_info "not ready yet ..."
exit 0

log_info "sync of jobs finished with $errors error(s) and $warnings warning(s)"
exit $errors
