#!/bin/bash
#
# run_etl.sh [jobId]
#
# Tom Gilissen
# Februari 2021

# shellcheck source=/dev/null
source functions.sh

debug=false

if [ -n "$1" ] && [ "$1" = "--debug" ]; then
  debug=true
fi
log_info "debug status: $debug"

log_info "not yet ready ..."


# Select the oldest job file
mapfile -t files < <(ls /data/etl/incoming/*.json -atr 2> /dev/null )
if [ ${#files[@]} -gt 0 ]; then
  job_file=${files[0]}
  job_file_name=${job_file#"/data/etl/incoming/"}
  log_info "job file selected: $job_file_name"
else
  log_debug "no job files: nothing to do"
  exit 0
fi

# Get data supplier and source directory
DATA_SUPPLIER=$( jq '.data_supplier' "$job_file" )
SOURCE_DIR=$( jq '.source_directory' "$job_file" )

log_info "data supplier: $DATA_SUPPLIER"
log_info "source directory: $SOURCE_DIR"

# Start ETL
# java -cp ${classpath} "$JAVA_OPTS" ${root_package}.nsr.NsrImporter --jobId "$jobId" --sourceFiles "$sourceFiles"
