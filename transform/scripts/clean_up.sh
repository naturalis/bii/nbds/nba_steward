#!/bin/bash
#
# clean_up.sh : remove all stopped containers
#
# Tom Gilissen
# Februari 2021

# shellcheck source=/dev/null
source functions.sh

if ! docker container prune -f > /dev/null 2>&1;
then
	log_warning "could not remove all stopped containers"
fi
