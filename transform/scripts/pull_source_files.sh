#!/bin/bash
# pull_source_files.sh
#
# Usage: ./pull_source_files.sh [job_file_name]
# E.g.: ./pull_source_files.sh nsr-2020-07-28-145709807-0134af9b.json
#
# Tom Gilissen
# Februari 2021


# shellcheck source=/dev/null
source functions.sh

alias aws='docker run --rm -i -v $STEWARD_DATA_DIR:/aws '\
'--env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID '\
'--env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY '\
'--env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION '\
'--env AWS_BUCKET=$AWS_S3_BUCKET '\
'amazon/aws-cli'

shopt -s expand_aliases

### Constants and variables

debug=false
warn=false
fail_on_warning=false
errors=0
warnings=0

job_file=


### Main

# Debug logging
if [ -n "$2" ] && [ "$2" = "--debug" ]; then
  debug=true
  warn=true
fi
if [ -n "$2" ] && [ "$2" = "--warn" ]; then
  warn=true
fi
if [ $debug ]; then
  log_debug "debug: $debug"
  log_debug "warn: $warn"
  log_debug "fail on warning: $fail_on_warning"
fi

begin=$(date +"%Y-%m-%d %T")
log_info "started at $begin"

# job file
if [ -z "$1" ]; then
  exit_on_error "Missing job file. Please provide the file name of the job"
fi
job_file_name="$1"
job_file="$STEWARD_DATA_DIR/new/$job_file_name"
log_debug "job file: $job_file"

if [ ! -e "$job_file" ]; then
  exit_on_error "File does not exist: $job_file! Please provide the file name of the job"
fi



# add inprogress extension to local file
mv "$job_file" "$STEWARD_DATA_DIR/new/$job_file_name.inprogress"
job_file="$job_file.inprogress"
log_debug "job file in progress: $job_file"

# retrieve supplier info
supplier=$( jq --raw-output '.data_supplier' < "$job_file" )
log_debug "data supplier: $supplier"

# sync source file directory on aws s3 to local file system
dir=$( jq --raw-output '.source_directory' < "$job_file")
if [ -z "$dir" ]; then
  exit_on_error "Missing source directory! Check the job file: $job_file"
fi

log_debug "synchronising directory: $dir to: $STEWARD_DATA_DIR/new/$dir"
if ! aws s3 sync "$AWS_S3_BUCKET/etl/$dir/" "./new/$dir";
then
  exit_on_error "sync of source folder $dir failed"
fi

# move job file to etl
mv "$STEWARD_DATA_DIR/new/$dir" "$ETL_DATA_DIR/new/$dir"
mv "$job_file" "$ETL_DATA_DIR/new/$job_file_name"

end=$(date +"%Y-%m-%d %T")
log_info "synced source file directory $dir"
log_info "job $job_file_name has been moved to $ETL_DATA_DIR/new/ and is now ready for further processing"
log_info "finished at $end with $errors error(s) and $warnings warning(s)"
exit $errors
