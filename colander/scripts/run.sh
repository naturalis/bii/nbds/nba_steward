#!/bin/bash
# 
# run.sh : utility scripts for running the three main tasks
# of this Steward: 
# pull_repo.sh     to retrieve the configuration
# deploy_dirs.sh   to create or update the directory structure
# prepare_jobs.sh  to check for new data and create jobs if there is
#
# Usage: ./run.sh
# 
# Extra parameter:
# --debug : show debug messages in log
# 
# 
# Tom Gilissen
# Januari 2021

source functions.sh

debug=false
if [ ! -z $1 ] && [ $1 = "--debug" ]; then
  debug=true
fi

log_info "Steward 1/6: pull config repo"
pull_repo.sh $1

log_info "Steward 2/6: deploy directories"
#deploy_dirs.sh $1
log_info "skipped"

log_info "Steward 3/6: pull new jobs"
pull_new_jobs.sh $1

log_info "Steward 4/6: move job to validator"
move_job_to_validator.sh $1

log_info "Steward 5/6: move job to infuser"
move_job_to_infuser.sh $1

log_info "Steward 6/6: remove stopped containers"
clean_up.sh