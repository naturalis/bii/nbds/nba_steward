#!/bin/bash
# pull_new_jobs
#
# NOTE: '-v $STEWARD_DATA:/aws' maps the directory of the host system 
# (NOT the steward container!) to the working directory /aws of the aws cli 
# container. However, in order to have access to the data folder inside this 
# steward container, we need to map that host directory to a directory in
# this container as well (we'll use: /data).
#
# Tom Gilissen
# Februari 2021

source functions.sh

### Constants and variables

# required .env variable:
# STEWARD_DATA=/data/steward

# for testing only
# source /data/.env

alias aws='docker run --rm -i -v $STEWARD_DATA:/aws \
         --env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID \
         --env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY \
         --env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION \
         --env AWS_BUCKET=$AWS_S3_BUCKET \
         amazon/aws-cli'

shopt -s expand_aliases

errors=0
warnings=0
debug=true


### Main

begin=`date +"%Y-%m-%d %T"`
log_info "started at $begin"

if [ -z $STEWARD_DATA ]; then
	exit_on_error "missing variable: STEWARD_DATA"
fi

# sync the AWS S3 new jobs folder to: /data/steward/incoming
error=$( aws s3 sync $AWS_S3_BUCKET/new/ incoming/ )
if [ $? -gt 0 ]; then
	exit_on_error "sync of new jobs folder failed: $error"
fi

# ... and move the job file(s) to the AWS S3 bucker "in_progress"
files=( $(ls $STEWARD_DATA/incoming/*.json -atr 2> /dev/null) )
if [ ${#files[@]} -gt 0 ]; then
	for file in ${files[@]}
	do
		file_name=${file#"$STEWARD_DATA/incoming/"}
		from="$AWS_S3_BUCKET/new/$file_name"
		to="$AWS_S3_BUCKET/in_progress/$file_name"
		error=$( aws s3 mv $from $to )
		if [ $? -gt 0 ]; then
			# TODO
			log_debug "moving job $file_name to the AWS bucket folder \"in_progress\" failed: $error"
			log_debug "Has the file already been moved?"
		else
			log_debug "moved job $file_name to the AWS bucket folder \"in_progress\""
		fi
	done	
else
	log_debug "no job files: nothing to do"
fi

end=`date +"%Y-%m-%d %T"`
log_info "sync of jobs finished at $end with $errors error(s) and $warnings warning(s)"
exit $errors
