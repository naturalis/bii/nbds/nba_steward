#!/bin/bash
# move_job_to_infuser.sh
#
# Tom Gilissen
# Februari 2021


source functions.sh

### Constants and variables

debug=true
warn=false
errors=0
warnings=0
fail_on_warning=false

job_file=
job_file_name=

### Main

begin=`date +"%Y-%m-%d %T"`
log_info "started at $begin"

# Extra logging
if [ ! -z $1 ] && [ $1 = "--debug" ]; then
	debug=true
	warn=true
fi
if [ ! -z $1 ] && [ $1 = "--warn" ]; then
	warn=true
fi

# Required environment variables
# VALIDATOR_OUTGOING_JOB_FOLDER
if [ -z $VALIDATOR_OUTGOING_JOB_FOLDER ]; then
	exit_on_error "missing variable: VALIDATOR_OUTGOING_JOB_FOLDER"
fi
# VALIDATOR_VALIDATED_OUTPUT_FOLDER
if [ -z $VALIDATOR_VALIDATED_OUTPUT_FOLDER ]; then
	exit_on_error "missing variable: VALIDATOR_VALIDATED_OUTPUT_FOLDER"
fi
if [ -z $INFUSER_DATA ]; then
	exit_on_error "missing variable: INFUSER_DATA"
fi


# Select the oldest job file
files=( $(ls $VALIDATOR_OUTGOING_JOB_FOLDER/*.json -atr 2> /dev/null) )
if [ ${#files[@]} -gt 0 ]; then
	job_file=${files[0]}
	job_file_name=${job_file#"$VALIDATOR_OUTGOING_JOB_FOLDER/"}
	log_info "job file selected: $job_file_name"
else
	log_debug "no job files: nothing to do"
	end=`date +"%Y-%m-%d %T"`
	log_info "finished at $end with $errors error(s) and $warnings warning(s)"
	exit 0
fi


# add inprogress extension to local file
mv $job_file $job_file".inprogress"
job_file=$job_file".inprogress"
log_debug "job file in progress: $job_file"


# move data files to infuser
data_files=( $(cat $job_file | jq --compact-output --raw-output '.validated_output | .[]') )
for data_file in ${data_files[@]}
do
	data_file_name=${data_file#"$VALIDATOR_VALIDATED_OUTPUT_FOLDER/"}
	mv $VALIDATOR_VALIDATED_OUTPUT_FOLDER/$data_file_name $INFUSER_DATA/incoming/$data_file_name || log_error "Failed to move data file $data_file"
done


# move the job file to the infuser job folder
log_debug "moving job file to validator: $VALIDATOR_INCOMING_JOB_FOLDER/$job_file_name"
mv $job_file $INFUSER_DATA/jobs/$job_file_name || log_error "Failed to move job file: $job_file"


end=`date +"%Y-%m-%d %T"`
log_info "moved job $job_file_name to the infuser for further processing"
log_info "finished at $end with $errors error(s) and $warnings warning(s)"
exit $errors
