#!/bin/bash
# move_job_to_validator.sh
#
# Tom Gilissen
# Februari 2021


source functions.sh

### Constants and variables

# required .env variables:
# STEWARD_DATA=/data/steward
# VALIDATOR_INCOMING_JOB_FOLDER=/data/validator/incoming

# for testing only
# source /data/.env

alias aws='docker run --rm -i -v $STEWARD_DATA:/aws \
         --env AWS_ACCESS_KEY_ID=$AWS_S3_ACCESS_KEY_ID \
         --env AWS_SECRET_ACCESS_KEY=$AWS_S3_SECRET_ACCESS_KEY \
         --env AWS_DEFAULT_REGION=$AWS_S3_DEFAULT_REGION \
         --env AWS_BUCKET=$AWS_S3_BUCKET \
         amazon/aws-cli'

shopt -s expand_aliases

debug=true
warn=false
errors=0
warnings=0
fail_on_warning=false

job_file=
job_file_name=

REPO_DIR=/etc/steward/config


### Main

begin=`date +"%Y-%m-%d %T"`
log_info "started at $begin"

# Extra logging
if [ ! -z $1 ] && [ $1 = "--debug" ]; then
	debug=true
	warn=true
fi
if [ ! -z $1 ] && [ $1 = "--warn" ]; then
	warn=true
fi

# set config repo
if [ -z $STEWARD_CONFIG_REPO_FILE ]; then
	exit_on_error "missing variable: STEWARD_CONFIG_REPO_FILE"
fi
CONFIG_REPO_FILE=$REPO_DIR/$STEWARD_CONFIG_REPO_FILE

if [ -z $STEWARD_DATA ]; then
	exit_on_error "missing variable: STEWARD_DATA"
fi
if [ -z $VALIDATOR_INCOMING_JOB_FOLDER ]; then
	exit_on_error "missing variable: VALIDATOR_INCOMING_JOB_FOLDER"
fi

# Select the oldest job file
files=( $(ls $STEWARD_DATA/incoming/*.json -atr 2> /dev/null) )
if [ ${#files[@]} -gt 0 ]; then
	job_file=${files[0]}
	job_file_name=${job_file#"$STEWARD_DATA/incoming/"}
	log_info "job file selected: $job_file_name"
else
	log_debug "no job files: nothing to do"
	end=`date +"%Y-%m-%d %T"`
	log_info "finished at $end with $errors error(s) and $warnings warning(s)"
	exit 0
fi

# add inprogress extension to local file
mv $job_file $job_file".inprogress"
job_file=$job_file".inprogress"
log_debug "job file in progress: $job_file"

# retrieve supplier info
supplier=$( cat $job_file | jq --raw-output '.data_supplier' )
log_debug "data supplier: $supplier"

# sync aws s3 directories to local file system
doc_types=( $(cat $job_file | jq --compact-output --raw-output '.source_directories | keys | .[]') )
for doc_type in ${doc_types[@]}
do
	log_debug "doc_type: $doc_type"
	dir=$( cat $job_file | jq --raw-output '.source_directories.'$doc_type )
	log_debug "dir: $dir"
	# Sync the AWS S3 new jobs folder to: /data/steward/incoming/$dir
	log_debug "synchronising directory: $dir to: $STEWARD_DATA/incoming/$dir"
	aws s3 sync $AWS_S3_BUCKET/$dir/ ./incoming/$dir
	if [ $? -gt 0 ]; then
		exit_on_error "sync of source folder $dir failed: $error"
	fi
done


# move source file directories to the validator directories
for doc_type in ${doc_types[@]}
do
        log_debug "doc_type: $doc_type"
        dir=$( cat $job_file | jq --raw-output '.source_directories.'$doc_type )
        log_debug "dir: $dir"
        log_debug "moving $dir to validator: $VALIDATOR_INCOMING_JOB_FOLDER/$doc_type/"
        mkdir $VALIDATOR_INCOMING_JOB_FOLDER/$dir -p
        mv $STEWARD_DATA/incoming/$dir/* $VALIDATOR_INCOMING_JOB_FOLDER/$dir/.
        if [ $? -gt 0 ]; then
                exit_on_error "moving source folder $dir failed: $error"
        fi
        touch $VALIDATOR_INCOMING_JOB_FOLDER/$dir/upload_ready
done


# remove the empty source file directory
UUID=${dir%%/*}
log_debug "remove dir: $STEWARD_DATA/incoming/$UUID"
rm -R $STEWARD_DATA/incoming/$UUID

# copy the job file to the validator
log_debug "adding job file to validator: $VALIDATOR_INCOMING_JOB_FOLDER/$job_file_name"
cp $job_file $VALIDATOR_INCOMING_JOB_FOLDER/$job_file_name

# ... and move it to the local in_progress directory
log_debug "backup $job_file to $STEWARD_DATA/in_progress/$job_file_name"
mkdir $STEWARD_DATA/in_progress -p
mv $job_file $STEWARD_DATA/in_progress/$job_file_name

end=`date +"%Y-%m-%d %T"`
log_info "moved job $job_file_name to the validator for further processing"
log_info "finished at $end with $errors error(s) and $warnings warning(s)"
exit $errors
