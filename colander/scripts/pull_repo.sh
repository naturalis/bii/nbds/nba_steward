#!/bin/bash
#
# pull_repo.sh : script for pulling the repo passed as 
# environment variable GITLAB_CONFIG_REPO
#
# Tom Gilissen
# Februari 2021

source functions.sh

mkdir -p /etc/steward

# checkout repo but empty directory first so clone will not fail
if [ ! -d /etc/steward/config/.git ]
then
  rm -rf /etc/steward/config/*
fi

# clone or pull
git clone ${GITLAB_CONFIG_REPO} /etc/steward/config 2> /dev/null || git -C /etc/steward/config reset --hard && git -C /etc/steward/config pull
