#!/bin/bash
#
# download_source_files.sh - After a job has been initiated, the source files
# need to be collected. To do this, download_source_files.sh needs two pieces
# of information from the job file:
# - the source system
# - the names of the source files
#
# The input for download_source_files.sh is the jobfile
# E.g.:
# ./download_source_files.sh crs-2020-03-26-092633599-579a126e.json
# 
# The script will then look for this job in the correct folder, read the file
# and start downloading the source files.
#
# Author: Tom Gilissen
# Date: April 2020 


##### Contstants and Variables

job_id=
declare -a files=()

job_file=
job_file_orig=
job_file_dest=
data_supplier=

outset_jobs=/data/colander/outset/jobs
outset_jobs_archive=/data/colander/outset/jobs_archive
outset_source_files=/data/colander/outset/source_files

etl_jobs=/data/colander/etl/jobs
etl_jobs_archive=/data/colander/etl/jobs_archive
etl_source_files=/data/colander/etl/source_files

validator_jobs=/data/colander/validator/jobs
validator_jobs_archive=/data/colander/validator/jobs_archive
validator_source_files=/data/colander/validator/source_files

declare -a source_files=()
declare -a source_files_specimen=()
declare -a source_files_taxon=()
declare -a source_files_multimedia=()
declare -a source_files_geoarea=()

declare -a validator_source_files_array=()

##### Functions

validate_json() {
	cat $1 | jq type &> /dev/null || exit_on_error "Failed to parse file: $1 (Invalid JSON)! Aborting"
}

exit_on_error() {
	echo "ERROR: $1" 1>&2 # send standard output to standard error
	mv $job_file $job_file_orig".failed"
	exit 1
}

set_job_file() {
	job_file_orig=$1
	job_file=$outset_jobs/$job_file_orig
	if [ ! -r $job_file ]
	then
		echo "ERROR: job file does not exist or cannot be read!"
		echo "Please make sure the job file is in the directory: $outset_jobs"
		exit 1
	fi
}

set_job_inprogress() {
	echo "$job_file is in progress"
	mv $job_file "$job_file.inprogress"
	job_file="$job_file.inprogress"
}

set_job_id() {
	job_id=$(jq -r '.id' $job_file) || exit_on_error "Failed to retrieve id from job file: $job_file_orig"
	echo "INFO: Job id: $job_id"
}

set_data_supplier() {
	data_supplier=$( (jq -r '.data_supplier' $job_file) | tr '[:upper:]' '[:lower:]' )
	echo "INFO: Data supplier: $data_supplier"
}

set_source_files() {
	source_files=$( jq -r '.source_files | @tsv' $job_file)
	echo "INFO: Source files: $( printf "\"%s\"\n" ${source_files[@]} | jq --compact-output -s '.' )"
}

set_source_files_archive() {
	
	# Retrieve the source files per doctype from the job file
	keys_json=$(jq -c -r ".source_files | keys | @tsv" $job_file)
	local -a keys=( $(echo $keys_json | tr "\t" "\n") )
	local file_size=0

	# Assign the source files to the matching array
	for key in ${keys[@]}
	do
    	local source_files_json=$(jq -c -r ".source_files.$key | @tsv" $job_file)
    	case $key in
    		"specimen")
				source_files_specimen=( $(echo $source_files_json | tr "\t" "\n") )
				let file_size+=${#source_files_specimen[*]}
				;;
			"taxon")
				source_files_taxon=( $(echo $source_files_json | tr "\t" "\n") )
				let file_size+=${#source_files_taxon[*]}
				;;
			"multimedia")
				source_files_multimedia=( $(echo $source_files_json | tr "\t" "\n") )
				let file_size+=${#source_files_multimedia[*]}
				;;
			"geoarea")
				source_files_geoarea=( $(echo $source_files_json | tr "\t" "\n") )
				let file_size+=${#source_files_geoarea[*]}
				;;
			*)
				echo "WARNING: Unknown document type: $key. File(s) will be ignored!"
				;;
			esac
	done

	# Check how many source files there are
	if [ $file_size -lt 1 ]
	then 
		echo "WARNING: Job file does not contain source files."
		echo "WARNING: No files have been downloaded."
		mv $job_file $job_file_orig".idle"
		echo "INFO: Job has finished."
		exit 0
	fi

	# Report which source files have been found per doctype
	if [[ $source_files_specimen != "" ]]
	then
		echo "INFO: specimen source files:"
		printf "\055 %s\n" ${source_files_specimen[@]}
	fi
	if [[ $source_files_multimedia != "" ]]
	then
		echo "INFO: multimedia source files:"
		printf "\055 %s\n" ${source_files_multimedia[@]}
	fi
	
	if [[ $source_files_taxon != "" ]]
	then
		echo "INFO: taxon source files:"
		printf "\055 %s\n" ${source_files_taxon[@]}
	fi

	if [[ $source_files_geoarea != "" ]]
	then
		echo "INFO: geoarea source files:"
		printf "\055 %s\n" ${source_files_geoarea[@]}
	fi
}

download_source_files() {
	echo "INFO: downloading source files"
		case $data_supplier in
		brahms)
			job_file_dest=$etl_jobs
			download_brahms_files
			;;
		crs)
			job_file_dest=$etl_jobs
			download_crs_files
			;;
		geo)
			job_file_dest=$validator_jobs
			download_geo_files
			;;
		nsr)
			job_file_dest=$etl_jobs
			download_nsr_files
			;;
		obs)
			job_file_dest=$validator_jobs
			download_obs_files
			;;
		xc)
			job_file_dest=$validator_jobs
			download_xc_files
			;;
		*)
			exit_on_error "Unknown data supplier: $data_supplier"
			;;
	esac
}

download_brahms_files() {

	server=minio-naturalis-bron
	dir=brondata-brahms

	for file in ${source_files[@]}
	do
		# List file to verify if the file is available on the server
		mc ls $server/$dir/$file || exit_on_error "Cannot find file \"$file\"! Aborting"
		count=$(mc ls $server/$dir/$file | wc -l)
		
		if [ $count -gt 1 ] 
		then
		 	exit_on_error "Found more than one file matching file name: \"$file\"! Aborting"
		fi
		echo "$(mc ls $server/$dir/$file --json | jq -r '.key')"

		# Download the file
		echo "INFO: Downloading: $file"
		echo "DEBUG: mc cp $server/$dir/$file $outset_source_files/brahms/$file"
		mkdir -p $outset_source_files/brahms
		mc cp $server/$dir/$file $outset_source_files/brahms/$file || exit_on_error "Downloading of file \"$file\" failed! Aborting"
		ls $outset_source_files/brahms/$file > /dev/null || exit_on_error "Downloading of file \"$file\" failed! Aborting"
	done

	# Now move the files to the ETL folder
	for file in ${source_files[@]}
	do
		echo "INFO: Moving $file to ETL"
		mkdir -p $etl_source_files/brahms
		mv $outset_source_files/brahms/$file $etl_source_files/brahms/$file || exit_on_error "Failed to move NSR source file $file to the ETL directory! Aborting"
	done
	echo "INFO: source files have been moved to ETL"
}

download_crs_files() {

	server=minio-naturalis-bron
	dir=brondata-crs

	for file in ${source_files[@]}
	do
		# List file to verify if the file is available on the server
		mc ls $server/$dir/$file || exit_on_error "Cannot find file \"$file\"! Aborting"
		count=$(mc ls $server/$dir/$file | wc -l)
		
		if [ $count -gt 1 ] 
		then
		 	exit_on_error "Found more than one file matching file name: \"$file\"! Aborting"
		fi
		echo "$(mc ls $server/$dir/$file --json | jq -r '.key')"

		# Download the file
		echo "INFO: Downloading: $file"
		echo "DEBUG: mc cp $server/$dir/$file $outset_source_files/brahms/$file"
		mkdir -p $outset_source_files/crs
		mc cp $server/$dir/$file $outset_source_files/crs/$file || exit_on_error "Downloading of file \"$file\" failed! Aborting"
		ls $outset_source_files/crs/$file > /dev/null || exit_on_error "Downloading of file \"$file\" failed! Aborting"
	done

	# Now move the files to the ETL folder
	for file in ${source_files[@]}
	do
		echo "INFO: Moving $file to ETL"
		mkdir -p $etl_source_files/crs
		mv $outset_source_files/crs/$file $etl_source_files/crs/$file || exit_on_error "Failed to move NSR source file $file to the ETL directory! Aborting"
	done
	echo "INFO: source files have been moved to ETL"
}

download_geo_files() {

	server=minio-naturalis-bron
	dir=brondata-geo

	for file in ${source_files[@]}
	do
		# List file to verify if the file is available on the server
		mc ls $server/$dir/$file || exit_on_error "Cannot find file \"$file\"! Aborting"
		count=$(mc ls $server/$dir/$file | wc -l)
		
		if [ $count -gt 1 ] 
		then
		 	exit_on_error "Found more than one file matching file name: \"$file\"! Aborting"
		fi
		echo "$(mc ls $server/$dir/$file --json | jq -r '.key')"

		# Download the file
		echo "INFO: Downloading: $file"
		echo "DEBUG: mc cp $server/$dir/$file $outset_source_files/geo/$file"
		mkdir -p $outset_source_files/geo
		mc cp $server/$dir/$file $outset_source_files/geo/$file || exit_on_error "Downloading of file \"$file\" failed! Aborting"
		ls $outset_source_files/geo/$file > /dev/null || exit_on_error "Downloading of file \"$file\" failed! Aborting"
	done

	# Now move the files to the ETL folder
	for file in ${source_files[@]}
	do
		echo "INFO: Moving $file to Validator"
		mkdir -p $validator_source_files/geo
		mv $outset_source_files/geo/$file $validator_source_files/geo/$file || exit_on_error "Failed to move NSR source file $file to the ETL directory! Aborting"
		touch $validator_source_files/geo/upload_ready
	done
	echo "INFO: source files have been moved to ETL"
}

download_nsr_files() {

	server=minio-naturalis-bron
	dir=brondata-nsr

	for file in ${source_files[@]}
	do
		# List file to verify if the file is available on the server
		mc ls $server/$dir/$file || exit_on_error "Cannot find file \"$file\"! Aborting"
		count=$(mc ls $server/$dir/$file | wc -l)
		
		if [ $count -gt 1 ] 
		then
		 	exit_on_error "Found more than one file matching file name: \"$file\"! Aborting"
		fi
		echo "$(mc ls $server/$dir/$file --json | jq -r '.key')"

		# Download the file
		echo "INFO: Downloading: $file"
		echo "DEBUG: mc cp $server/$dir/$file $outset_source_files/nsr/$file"
		mkdir -p $outset_source_files/nsr
		mc cp $server/$dir/$file $outset_source_files/nsr/$file || exit_on_error "Downloading of file \"$file\" failed! Aborting"
		ls $outset_source_files/nsr/$file > /dev/null || exit_on_error "Downloading of file \"$file\" failed! Aborting"
	done

	# Now move the files to the ETL folder
	for file in ${source_files[@]}
	do
		echo "INFO: Moving $file to ETL"
		mkdir -p $etl_source_files/nsr
		mv $outset_source_files/nsr/$file $etl_source_files/nsr/$file || exit_on_error "Failed to move NSR source file $file to the ETL directory! Aborting"
	done
	echo "INFO: source files have been moved to ETL"
}

download_xc_files() {
	server=minio-xc-bron
	dir=
	mkdir -p $outset_source_files/xc/{multimedia,specimen}
	mkdir -p $validator_source_files/xc/{multimedia,specimen}

	for file in ${source_files[@]}
	do
		# Specimen or multimedia file?
		if [[ $file == *"specimen"* ]] ; then
			dir="specimen"
		else
			dir="multimedia"
		fi

		# List file to verify if the file is available on the server
		mc ls $server/$dir/$file || exit_on_error "Cannot find file \"$file\"! Aborting"
		count=$(mc ls $server/$dir/$file | wc -l)
		
		if [ $count -gt 1 ] 
		then
		 	exit_on_error "Found more than one file matching file name: \"$file\"! Aborting"
		fi
		echo "DEBUG: $(mc ls $server/$dir/$file --json | jq -r '.key')"

		# Download the file
		echo "INFO: Downloading: $file"
		echo "DEBUG: mc cp $server/$dir/$file $outset_source_files/xc/$dir/$file"
		
		mc cp $server/$dir/$file $outset_source_files/xc/$dir/$file || exit_on_error "Downloading of file \"$file\" failed! Aborting"
		ls $outset_source_files/xc/$dir/$file > /dev/null || exit_on_error "Downloading of file \"$file\" failed! Aborting"
	done

	# Now move the files to the Validator folder
	for file in ${source_files[@]}
	do
		# Specimen of multimedia file?
		if [[ $file == *"specimen"* ]] ; then
			dir="specimen"
		else
			dir="multimedia"
		fi
		echo "INFO: Moving $file to Validator"
		mv $outset_source_files/xc/$dir/$file $validator_source_files/xc/$dir/$file || exit_on_error "Failed to move XC source file $file to the Validator directory! Aborting"
		validator_source_files_array+=("source_files/xc/$dir/$file")
	done
	echo "DEBUG: array: ${validator_source_files_array[@]}"
	echo "INFO: source files have been moved to Validator"
}

download_obs_files() {
	server=minio-obs-bron
	echo "Downloading of waarneming files has not been implemented yet"
}

update_job_details() {
	if [[ ${#validator_source_files_array[*]} -gt 0 ]]
	then
		echo "INFO: Updating $job_file_orig"
		local str=$( printf "\"%s\"\n" ${validator_source_files_array[@]} | jq  --compact-output -s '.' )
		cat $job_file | jq '. + {"validator_source_files": '$str'}' > $job_file.tmp
		mv $job_file.tmp $job_file		 
	fi
}

update_job_status() {
	echo "INFO: Updating status"
	cat $job_file | jq 'to_entries | map( if .key == "status" then {"key":"status", "value":"source files downloaded"} else . end ) | from_entries' > $job_file.tmp
	mv $job_file.tmp $job_file
}

reset_job_status() {
	# Remove the .inprogress extension from the file name 
	mv $job_file $job_file_dest/$job_file_orig || exit_on_error "Failed to reset job file from \"$job_file\" to \"$job_file_orig\"! Aborting"
	# echo "INFO: $job_file_orig is ready"
}

usage() {
	echo "INFO: Usage: ./download_source_files.sh [job-id].json"
}


##### Main

args_count=$#
if [ $args_count -eq 0 ] || [ $args_count -gt 1 ]
then
	echo "Number of parameters does not match." 
	usage
	exit 1
fi

set_job_file $1
set_job_inprogress
validate_json $job_file

set_job_id
set_data_supplier
set_source_files

download_source_files

update_job_details

update_job_status
reset_job_status

echo "INFO: Finished downloading source file(s) for job with id: $job_id"